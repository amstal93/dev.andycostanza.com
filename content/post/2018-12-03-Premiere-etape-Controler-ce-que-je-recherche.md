---
author: Andy Costanza
comments: true
date: 2018-12-03 15:51:12+00:00
layout: post
link: http://andycostanza.gitlab.io/post/Premiere-etape-Controler-ce-que-je-recherche
slug: Premiere-etape-Controler-ce-que-je-recherche
title: Première étape - Contrôler ce que je recherche
tags:
 - google
 - moteur de recherche
 - startpage
 - monopole
 - dégooglisation
categories: 
 - Reprendre le contrôle
---

## TL;DR 
Faire une recherche sur Google de manière anonyme grâce à [Startpage.com](https://www.startpage.com)

<!--more-->

## Monopole naturel
Commençons par ce graphique : 

<img alt="Parts de marché des moteurs de recherche en novembre 2018. France, USA, monde" src="https://andycostanza.gitlab.io/images/2018-12-04/monopole-google.png" style="height:auto; width:100%" />

Source: [webrankinfo.com](https://www.webrankinfo.com/dossiers/etudes/parts-marche-moteurs)

C'est indéniable, nous avons contribué à cela. 

Jour après jour, recherche après recherche...

À grand renfort d'annonce en grande pompe, de coup de pub marketing et en surfant sur la vague du "Nous ne sommes pas les méchants", "Vous pouvez nous quitter quand vous voulez et reprendre toutes vos données" ou encore "Regardez comme on est cool, on a intégré un noyau GNU/Linux dans nos téléphones"

J'ai fait confiance. J'ai utilisé les services, TOUS les services. Et j'ai abandonné mes données, mes photos, mes déplacements, mes goûts, mes craintes, mes peurs, ma vie privée, recherche après recherche...

Il est difficile de se rendre compte du problème quand le problème devient la norme. Quand le nom d'une société devient un verbe, c'est qu'il est temps de s'inquiéter de l'impact qu'elle a sur nos vies.

Mais quand j'en parle autour de moi, on me regarde avec de grand yeux d'un air étonné et on me répond, "J'ai rien à cacher moi !". 

Et là, je passe pour un fou, un atypique, un paranoïaque

## Prise de conscience
Après avoir fait le tour des alternatives sur le marché des moteurs de recherches internet qui respectent ma vie privée, c'est avec [Startpage.com](https://www.startpage.com) que j'ai décidé de travailler.

Plusieurs points sont à mettre en évidence :
1. Mes recherches se font toujours sur Google, mais de manière anonyme, sans traqueurs, sans pubs et sans recommandations dans mes résultats
2. Je peux consulter le résultat d'une recherche tout en restant anonyme grâce à la fonction "__Affichage Anonyme__"
<img alt="Recherche sur Startpage.com" src="https://andycostanza.gitlab.io/images/2018-12-04/recherche1.png" style="height:auto; width:100%" />
<img alt="Consulter le résultat d'une recherche en restant anonyme" src="https://andycostanza.gitlab.io/images/2018-12-04/recherche2.png" style="height:auto; width:100%" />
3. Comme aucune de mes données n'est collectée je suis sûr qu'une recherche antérieur ne viendra pas biaisé le résultat de ma recherche actuelle
4. j'ai insatllé [leur plugin](https://www.startpage.com/fr/search/download-startpage-plugin.html) pour que la recherche automatique qui se fait dans ma barre de recherche se fasse directement par Startpage et plus via Google
<img alt="Paramètres Firefox" src="https://andycostanza.gitlab.io/images/2018-12-04/firefox-search.png" style="height:auto; width:100%" />

Bref, si toi aussi tu veux reprendre le contrôle de tes recherches sur internet sans plus jamais laisser de trace, alors clique sur cette image
[<img alt="Startpage.com Le moteur de recherche le plus privé au monde." src="https://andycostanza.gitlab.io/images/2018-12-04/startpage-full.png" style="height:auto; width:100%" />](https://www.startpage.com)