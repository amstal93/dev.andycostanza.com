---
author: Andy Costanza
comments: true
date: 2018-05-07 15:51:12+00:00
layout: post
link: http://andycostanza.gitlab.io/post/How-pagination-works-with-ng-bootstrap
slug: How-pagination-works-with-ng-bootstrap
title: How pagination works with ng-bootstrap
categories:
- dev
tags:
- angular
- bootstrap
- ng-bootstrap
---

## How it all began
...with the official documentation on https://ng-bootstrap.github.io/#/components/pagination/examples

OK, i can put the pagination under my table but how to paginate the result with my Angular component?

<!--more-->

## Now, the solution i've found
On [Stack Overflow](https://stackoverflow.com/a/44042531/2139192), i found how to implement the pagination to load my table


```ts
import {Component, OnInit} from '@angular/core';
import {WaterService} from '../service/water.service';
import {Water} from '../model/water';

@Component({
  selector: 'app-water',
  templateUrl: './water.component.html',
  styleUrls: ['./water.component.css']
})
export class WaterComponent implements OnInit {

  waterList: Water[];

  itemsPerPage: number=10;
  totalItems: any;
  page: any=1;
  previousPage: any;

  constructor(private waterService: WaterService) {}

  ngOnInit() {
    this.loadData();
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.loadData();
    }
  }

  loadData() {
    this.waterService.getAllPaginate(
      this.page - 1,
      this.itemsPerPage,
    ).subscribe(
      response => {
        this.waterList = response.content;
        this.totalItems = response.totalElements;
        this.itemsPerPage=response.size;
      },
      e => console.log(e)
    )
  }
}
```

```html
<div class="row">
	<div class="col-12">
    	<table class="table table-striped">
          <thead class="thead-dark">
          <th scope="col">Statement date</th>
          <th scope="col">Water statement index (m³)</th>
          <th></th>
          </thead>
          <tbody>
          <tr *ngFor="let e of waterList">
            <td>{{e.statementDate| date: 'dd/MM/yyyy'}}</td>
            <td>{{e.waterIndex}}</td>
            <th class="text-right">
              <a class="btn btn-primary"
                 routerLink="/water/{{e.id}}"
              >
                <i class="far fa-edit"></i></a>
              <button class="btn btn-danger" (click)="delete(e)">
                <i class="far fa-trash-alt"></i></button>
            </th>
          </tr>
          </tbody>
        </table>
        <div class="col-12 justify-content-center mb-5">
          <ngb-pagination [collectionSize]="totalItems"
          [pageSize]="itemsPerPage" 
          [(page)]="page" 
          [maxSize]="7" 
          [rotate]="true" 
          [boundaryLinks]="true" 
          (pageChange)="loadPage($event)"></ngb-pagination>
        </div>
</div>
```

## Tips to center the pagination in the middle of the table

```css
.justify-content-center {
  display: flex !important;
  justify-content: center !important;
}
```



